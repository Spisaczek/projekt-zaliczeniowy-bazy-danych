package com.kamilspisak.policedepartamentstorageapp.Departament;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.kamilspisak.policedepartamentstorageapp.Cop.CopsListActivity;
import com.kamilspisak.policedepartamentstorageapp.Evidence.EvidencesListActivity;
import com.kamilspisak.policedepartamentstorageapp.Probe.ProbesListActivity;
import com.kamilspisak.policedepartamentstorageapp.R;
import com.kamilspisak.policedepartamentstorageapp.Utils.Repository;
import com.kamilspisak.policedepartamentstorageapp.Utils.UserPrefs;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.SectionDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

public class DepartamentList extends AppCompatActivity {

    private Drawer drawer;
    private DepartamentController departamentController;
    private Repository repository;
    private ArrayList<Departament> departaments;
    private SharedPreferences settings;

    @Bind(R.id.listView)
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simple_list);
        ButterKnife.bind(this);
        departamentController = new DepartamentController(this);
        departaments = departamentController.read();

        ArrayAdapter<Departament> departamentAdapter= new ArrayAdapter<Departament>(this, R.layout.support_simple_spinner_dropdown_item, departaments);
        listView.setAdapter(departamentAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                UserPrefs us = new UserPrefs(getSharedPreferences(UserPrefs.PREFS_NAME, 0));
                us.setDepId(departaments.get(position).id);
                Toast.makeText(getBaseContext(), "Dep id: " + String.valueOf(us.getDepId()), Toast.LENGTH_SHORT).show();
            }
        });
        buildDrawer();
    }

    private void buildDrawer() {
        PrimaryDrawerItem departamentItem = new PrimaryDrawerItem().withName("Departaments").withIcon(FontAwesome.Icon.faw_building);
        PrimaryDrawerItem copsItem = new PrimaryDrawerItem().withName("Cops").withIcon(FontAwesome.Icon.faw_male);
        PrimaryDrawerItem probesItem = new PrimaryDrawerItem().withName("Probes").withIcon(FontAwesome.Icon.faw_suitcase);
        PrimaryDrawerItem evidencesItem = new PrimaryDrawerItem().withName("Evidences").withIcon(FontAwesome.Icon.faw_archive);

        AccountHeader headerResult = new AccountHeaderBuilder()
                .withActivity(this)
                .withHeaderBackground(R.drawable.polish_police_logo)
                .build();

        drawer = new DrawerBuilder()
                .withActivity(this)
                .withAccountHeader(headerResult)
                .withTranslucentStatusBar(false)
                .withActionBarDrawerToggle(true)
                .withActionBarDrawerToggleAnimated(true)
                .addDrawerItems(
                        departamentItem,
                        copsItem,
                        probesItem,
                        evidencesItem,
                        new SectionDrawerItem().withName("Other"),
                        new PrimaryDrawerItem().withName("Author").withIcon(FontAwesome.Icon.faw_info)
                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        Intent intent;
                        switch (position) {
                            case 1:
                                drawer.closeDrawer();
                                break;
                            case 2:
                                drawer.closeDrawer();
                                intent = new Intent(DepartamentList.this, CopsListActivity.class);
                                DepartamentList.this.startActivity(intent);
                                finish();
                                break;
                            case 3:
                                drawer.closeDrawer();
                                intent = new Intent(DepartamentList.this, ProbesListActivity.class);
                                DepartamentList.this.startActivity(intent);
                                finish();
                                break;
                            case 4:
                                drawer.closeDrawer();
                                intent = new Intent(DepartamentList.this, EvidencesListActivity.class);
                                DepartamentList.this.startActivity(intent);
                                finish();
                                break;
                        }
                        return true;
                    }
                })
                .withSelectedItem(-1)
                .build();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
}
