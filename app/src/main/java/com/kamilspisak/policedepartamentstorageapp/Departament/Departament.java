package com.kamilspisak.policedepartamentstorageapp.Departament;

/**
 * Created by Kamil on 2016-01-13.
 */
public class Departament {

    public int id;
    public String name;
    public String street;
    public int streetNumber;
    public String city;
    public String postCode;

    public Departament(int id, String name, String street, int streetNumber, String city, String postCode) {
        this.id = id;
        this.name = name;
        this.street = street;
        this.streetNumber = streetNumber;
        this.city = city;
        this.postCode = postCode;
    }

    @Override
    public String toString() {
        return name;
    }
}
