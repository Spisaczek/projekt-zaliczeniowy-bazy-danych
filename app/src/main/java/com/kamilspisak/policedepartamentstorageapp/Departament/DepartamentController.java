package com.kamilspisak.policedepartamentstorageapp.Departament;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.kamilspisak.policedepartamentstorageapp.Interfaces.ICrud;
import com.kamilspisak.policedepartamentstorageapp.Utils.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kamil on 2016-01-13.
 */
public class DepartamentController implements ICrud<Departament> {

    public static final String KEY_TABLE_NAME = "KOMISARIAT";
    public static final String KEY_ID = "_id";
    public static final String KEY_STREET = "Ulica";
    public static final String KEY_NUMBER = "Nr_budynku";
    public static final String KEY_CITY = "Miasto";
    public static final String KEY_POST_CODE = "Kod_pocztowy";

    private Repository repository;

    public DepartamentController(Context context) {
        repository = new Repository(context);
    }

    public Departament getDepartamentById(int id){
        Cursor cursor = repository.getReadableDatabase().rawQuery("SELECT * FROM " + KEY_TABLE_NAME + " WHERE " + KEY_ID + " = " + String.valueOf(id) + ";", null);
        return cursorToObject(cursor, 0);
    }

    private Departament cursorToObject(Cursor cursor, int index){
        cursor.moveToPosition(index);
        Departament departament = new Departament(
                cursor.getInt(0),
                cursor.getString(1),
                cursor.getString(2),
                cursor.getInt(3),
                cursor.getString(4),
                cursor.getString(5));
        return departament;
    }

    @Override
    public void create(Departament departament) {
        throw new UnsupportedOperationException("Cannot create new Departament yet");
    }

    @Override
    public void update(Departament departament) {
        throw new UnsupportedOperationException("Cannot update Departament yet");
    }

    @Override
    public void delete(Departament departament) {
        throw new UnsupportedOperationException("Cannot delete Departament yet");
    }

    @Override
    public ArrayList<Departament> read() {
        ArrayList<Departament> departaments = new ArrayList<>();

        Cursor cursor = repository.getReadableDatabase().rawQuery("SELECT * FROM " + KEY_TABLE_NAME + ";", null);
        for(int i = 0; i < cursor.getCount(); i++){
            Departament departament = cursorToObject(cursor, i);
            Log.i("Departament: ", departament.name);
            departaments.add(departament);
        }
        return departaments;
    }
}
