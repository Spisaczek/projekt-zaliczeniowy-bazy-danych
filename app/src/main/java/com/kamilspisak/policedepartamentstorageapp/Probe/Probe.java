package com.kamilspisak.policedepartamentstorageapp.Probe;

/**
 * Created by ksmal on 27.12.2015.
 */
public class Probe {

    public int id;
    public String name;
    public String description;

    public Probe(int id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    @Override
    public String toString() {
        return name;
    }
}
