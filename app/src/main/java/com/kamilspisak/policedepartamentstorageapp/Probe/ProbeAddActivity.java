package com.kamilspisak.policedepartamentstorageapp.Probe;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

import com.kamilspisak.policedepartamentstorageapp.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class ProbeAddActivity extends AppCompatActivity {

    ProbeController probeController;

    @Bind(R.id.nameText) EditText nameText;
    @Bind(R.id.descText) EditText descText;
    @Bind(R.id.submitBtn) Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_probe_add);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.submitBtn)
    void OnSubmitBtnClick(){
        Probe probe = new Probe(0, nameText.getText().toString(), descText.getText().toString());
        probeController = new ProbeController(this);
        probeController.create(probe);
        finish();
    }
}
