package com.kamilspisak.policedepartamentstorageapp.Probe;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.kamilspisak.policedepartamentstorageapp.Cop.Cop;
import com.kamilspisak.policedepartamentstorageapp.Cop.CopController;
import com.kamilspisak.policedepartamentstorageapp.Probe.CopHasProbe.CopHasProbe;
import com.kamilspisak.policedepartamentstorageapp.Probe.CopHasProbe.CopHasProbeController;
import com.kamilspisak.policedepartamentstorageapp.R;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Kamil on 2016-01-06.
 */
public class AddCopToProbeActivity extends AppCompatActivity {

    private ArrayAdapter<Cop> copsAdapter;
    private CopController copController;
    private ArrayList<Cop> cops;

    @Bind(R.id.listView) ListView listView;

    public AddCopToProbeActivity() {
        super();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simple_list);
        ButterKnife.bind(this);

        copController = new CopController(this);
        cops = copController.read();
        copsAdapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, cops);

        listView.setAdapter(copsAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                new CopHasProbeController(getBaseContext()).
                        create(new CopHasProbe(copsAdapter.getItem(position).id, getIntent().getIntExtra("id", 0)));
                finish();
            }
        });
    }

}
