package com.kamilspisak.policedepartamentstorageapp.Probe.CopHasProbe;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.kamilspisak.policedepartamentstorageapp.Utils.Repository;
import com.kamilspisak.policedepartamentstorageapp.Interfaces.ICrud;

import java.util.ArrayList;

/**
 * Created by ksmal on 27.12.2015.
 */
public class CopHasProbeController implements ICrud<CopHasProbe> {

    private final String KEY_TABLE_NAME = "funkcjonariusz_has_sledztwo";
    private final String KEY_COP_ID = "FUNKCJONARIUSZ_NR_ODZNAKI";
    private final String KEY_PROBE_ID = "SLEDZTWO_ID";

    private Repository repository;

    public CopHasProbeController(Context context){
        repository = new Repository(context);
    }

    @Override
    public void create(CopHasProbe copHasProbe) {
        ContentValues copContent = getContentValues(copHasProbe);
        repository.getWritableDatabase().insert(KEY_TABLE_NAME, null, copContent);
    }

    private ContentValues getContentValues(CopHasProbe copHasProbe) {
        ContentValues copHasProbeContent = new ContentValues();
        copHasProbeContent.put(KEY_COP_ID, copHasProbe.copId);
        copHasProbeContent.put(KEY_PROBE_ID, copHasProbe.probeId);
        return copHasProbeContent;
    }

    @Override
    public void update(CopHasProbe copHasProbe) {
        ContentValues caseContent = getContentValues(copHasProbe);
        repository.getWritableDatabase().
                update(KEY_TABLE_NAME, caseContent,
                        "WHERE " + KEY_COP_ID + " = " + String.valueOf(copHasProbe.copId) +
                                " AND " + KEY_PROBE_ID + " = " + String.valueOf(copHasProbe.probeId) + ";", null);
    }

    @Override
    public void delete(CopHasProbe copHasProbe) {
        repository.getWritableDatabase()
                .execSQL("DELETE FROM" + KEY_TABLE_NAME + "WHERE " + KEY_COP_ID + " = " + String.valueOf(copHasProbe.copId) +
                        " AND " + KEY_PROBE_ID + " = " + String.valueOf(copHasProbe.probeId) + ";");
    }

    @Override
    public ArrayList<CopHasProbe> read() {
        ArrayList<CopHasProbe> copHasProbes = new ArrayList<CopHasProbe>();
        Cursor cursor = repository.getReadableDatabase()
                .rawQuery("SELECT * FROM " + KEY_TABLE_NAME + ";", null);

        for (int i = 0; i < cursor.getCount(); i++){
            CopHasProbe copHasProbe = cursorToObject(cursor, i);
            //Log.i("----------Cop ", copHasProbe.name + ", id: " + String.valueOf(copHasProbe.id));
            copHasProbes.add(copHasProbe);
        }

        return copHasProbes;
    }

    private CopHasProbe cursorToObject(Cursor cursor, int index){
        cursor.moveToPosition(index);
        CopHasProbe copHasProbe = new CopHasProbe(
                cursor.getInt(0),
                cursor.getInt(1)
        );
        return copHasProbe;
    }

}
