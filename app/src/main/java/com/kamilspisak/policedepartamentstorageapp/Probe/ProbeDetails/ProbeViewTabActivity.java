package com.kamilspisak.policedepartamentstorageapp.Probe.ProbeDetails;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.kamilspisak.policedepartamentstorageapp.Interfaces.IRefreshableFragment;
import com.kamilspisak.policedepartamentstorageapp.Probe.AddCopToProbeActivity;
import com.kamilspisak.policedepartamentstorageapp.Probe.AddEvidenceToProbeActivity;
import com.kamilspisak.policedepartamentstorageapp.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ProbeViewTabActivity extends AppCompatActivity {

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private int probeId;

    @Bind(R.id.container) ViewPager mViewPager;
    @Bind(R.id.fab) FloatingActionButton fab;
    @Bind(R.id.tabs) TabLayout tabLayout;
    @Bind(R.id.toolbar) Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cop_details_tab);
        ButterKnife.bind(this);
        probeId = getIntent().getIntExtra("id", 1);

        setSupportActionBar(toolbar);

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        fab.hide();
                        break;
                    case 1:
                        if (!fab.isShown()) fab.show();
                        fab.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent newIntent = new Intent(ProbeViewTabActivity.this, AddCopToProbeActivity.class);
                                newIntent.putExtra("id", probeId);
                                ProbeViewTabActivity.this.startActivity(newIntent);
                            }
                        });
                        break;
                    case 2:
                        if (!fab.isShown()) fab.show();
                        fab.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent newIntent = new Intent(ProbeViewTabActivity.this, AddEvidenceToProbeActivity.class);
                                newIntent.putExtra("id", probeId);
                                ProbeViewTabActivity.this.startActivity(newIntent);
                            }
                        });
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        tabLayout.setupWithViewPager(mViewPager);
        fab.hide();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_probe_viev_tab, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return ProbeDetailsMainFragment.newInstance(probeId);
                case 1:
                    return CopsListFragment.newInstance(probeId);
                case 2:
                    return EvidencesListFragment.newInstance(probeId);
            }
            return null;
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Details";
                case 1:
                    return "Cops";
                case 2:
                    return "Evidences";
            }
            return null;
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) refresh();
    }

    private void refresh(){
        for(Fragment fragment : getSupportFragmentManager().getFragments()){
            if(fragment instanceof IRefreshableFragment) ((IRefreshableFragment) fragment).refresh();
        }
    }
}
