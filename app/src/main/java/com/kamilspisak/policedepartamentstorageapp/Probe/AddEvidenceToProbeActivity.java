package com.kamilspisak.policedepartamentstorageapp.Probe;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.kamilspisak.policedepartamentstorageapp.Evidence.Evidence;
import com.kamilspisak.policedepartamentstorageapp.Evidence.EvidenceController;
import com.kamilspisak.policedepartamentstorageapp.Probe.ProbeHasEvidence.ProbeHasEvidence;
import com.kamilspisak.policedepartamentstorageapp.Probe.ProbeHasEvidence.ProbeHasEvidenceController;
import com.kamilspisak.policedepartamentstorageapp.R;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Kamil on 2016-01-06.
 */
public class AddEvidenceToProbeActivity extends AppCompatActivity {

    private EvidenceController evidenceController;
    private ArrayList<Evidence> evidences;
    private ArrayAdapter<Evidence> evidencesAdapter;

    @Bind(R.id.listView) ListView listView;

    public AddEvidenceToProbeActivity() {
        super();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simple_list_with_action_button);
        ButterKnife.bind(this);

        evidenceController = new EvidenceController(this);
        evidences = evidenceController.read();

        evidencesAdapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, evidences);
        listView.setAdapter(evidencesAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                new ProbeHasEvidenceController(getBaseContext())
                        .create(new ProbeHasEvidence(getIntent().getIntExtra("id", 0), evidencesAdapter.getItem(position).id));
                Log.i("KKK", String.valueOf(evidencesAdapter.getItem(position).id));
                finish();
            }
        });
    }
}
