package com.kamilspisak.policedepartamentstorageapp.Probe.ProbeHasEvidence;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.kamilspisak.policedepartamentstorageapp.Utils.Repository;
import com.kamilspisak.policedepartamentstorageapp.Interfaces.ICrud;

import java.util.ArrayList;

/**
 * Created by Kamil on 2016-01-06.
 */
public class ProbeHasEvidenceController implements ICrud<ProbeHasEvidence> {

    private final String KEY_TABLE_NAME = "SLEDZTWO_HAS_DOWOD";
    private final String KEY_EVIDENCE_ID = "DOWOD_id";
    private final String KEY_PROBE_ID = "SLEDZTWO_ID";

    private Repository repository;

    public ProbeHasEvidenceController(Context context) {
        this.repository = new Repository(context);
    }

    @Override
    public void create(ProbeHasEvidence probeHasEvidence) {
        ContentValues copContent = getContentValues(probeHasEvidence);
        repository.getWritableDatabase().insert(KEY_TABLE_NAME, null, copContent);
    }

    private ContentValues getContentValues(ProbeHasEvidence probeHasEvidence) {
        ContentValues probeHasEvidenceContent = new ContentValues();
        probeHasEvidenceContent.put(KEY_EVIDENCE_ID, probeHasEvidence.evidenceID);
        probeHasEvidenceContent.put(KEY_PROBE_ID, probeHasEvidence.probeID);
        return probeHasEvidenceContent;
    }

    @Override
    public void update(ProbeHasEvidence probeHasEvidence) {
        ContentValues caseContent = getContentValues(probeHasEvidence);
        repository.getWritableDatabase().
                update(KEY_TABLE_NAME, caseContent,
                        "WHERE " + KEY_EVIDENCE_ID + " = " + String.valueOf(probeHasEvidence.evidenceID) +
                                " AND " + KEY_PROBE_ID + " = " + String.valueOf(probeHasEvidence.probeID) + ";", null);
    }

    @Override
    public void delete(ProbeHasEvidence probeHasEvidence) {
        repository.getWritableDatabase()
                .execSQL("DELETE FROM" + KEY_TABLE_NAME + "WHERE " + KEY_EVIDENCE_ID + " = " + String.valueOf(probeHasEvidence.evidenceID) +
                        " AND " + KEY_PROBE_ID + " = " + String.valueOf(probeHasEvidence.probeID) + ";");
    }

    @Override
    public ArrayList<ProbeHasEvidence> read() {
        ArrayList<ProbeHasEvidence> copHasProbes = new ArrayList<ProbeHasEvidence>();
        Cursor cursor = repository.getReadableDatabase()
                .rawQuery("SELECT * FROM " + KEY_TABLE_NAME + ";", null);

        for (int i = 0; i < cursor.getCount(); i++){
            ProbeHasEvidence probeHasEvidence = CursorToObject(cursor, i);
            //Log.i("----------Cop ", copHasProbe.name + ", id: " + String.valueOf(copHasProbe.id));
            copHasProbes.add(probeHasEvidence);
        }

        return copHasProbes;
    }

    private ProbeHasEvidence CursorToObject(Cursor cursor, int index){
        cursor.moveToPosition(index);
        ProbeHasEvidence copHasProbe = new ProbeHasEvidence(
                cursor.getInt(0),
                cursor.getInt(1)
        );
        return copHasProbe;
    }
}
