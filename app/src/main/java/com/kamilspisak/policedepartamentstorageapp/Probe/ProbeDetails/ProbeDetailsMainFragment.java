package com.kamilspisak.policedepartamentstorageapp.Probe.ProbeDetails;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kamilspisak.policedepartamentstorageapp.Probe.Probe;
import com.kamilspisak.policedepartamentstorageapp.Probe.ProbeController;
import com.kamilspisak.policedepartamentstorageapp.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ProbeDetailsMainFragment extends Fragment {

    private static final String ARG_PROBE_ID = "section_number";

    private int probeId;

    @Bind(R.id.card_name_label) TextView nameTextView;
    @Bind(R.id.card_desc_text) TextView descTextView;

    public ProbeDetailsMainFragment() {
    }

    public static ProbeDetailsMainFragment newInstance(int probeId) {
        ProbeDetailsMainFragment fragment = new ProbeDetailsMainFragment();

        Bundle args = new Bundle();
        args.putInt(ARG_PROBE_ID, probeId);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        probeId = getArguments().getInt(ARG_PROBE_ID);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_probe_main, container, false);
        ButterKnife.bind(this, view);

        ProbeController probeController = new ProbeController(container.getContext());
        Probe probe = probeController.getProbeByID(probeId);
        nameTextView.setText(probe.name);
        descTextView.setText(probe.description);

        return view;
    }
}
