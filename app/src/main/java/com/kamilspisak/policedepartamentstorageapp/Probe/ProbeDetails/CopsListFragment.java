package com.kamilspisak.policedepartamentstorageapp.Probe.ProbeDetails;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.kamilspisak.policedepartamentstorageapp.Cop.Cop;
import com.kamilspisak.policedepartamentstorageapp.Cop.CopController;
import com.kamilspisak.policedepartamentstorageapp.Utils.Repository;
import com.kamilspisak.policedepartamentstorageapp.Interfaces.IRefreshableFragment;
import com.kamilspisak.policedepartamentstorageapp.R;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link CopsListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CopsListFragment extends Fragment implements IRefreshableFragment{

    private static final String ARG_PROBE_ID = "PROBE_ID";

    private ArrayAdapter<Cop> copsAdapter;
    private int probeId;
    private CopController copController;

    @Bind(R.id.listView) ListView listView;

    public CopsListFragment() {
        // Required empty public constructor
    }

    public static CopsListFragment newInstance(int probeId) {
        CopsListFragment fragment = new CopsListFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PROBE_ID, probeId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        probeId = getArguments().getInt(ARG_PROBE_ID);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_simple_list, container, false);
        ButterKnife.bind(this, view);

        fillListView(listView);
        registerForContextMenu(listView);
        return view;
    }

    private void fillListView(ListView listView) {
        String selectQuery = "SELECT * FROM funkcjonariusz_has_sledztwo WHERE SLEDZTWO_ID = " + String.valueOf(probeId) + ";";
        Cursor cursor = new Repository(getContext())
                .getReadableDatabase()
                .rawQuery(selectQuery, null);

        copController = new CopController(getContext());
        ArrayList<Cop> cops = new ArrayList<>();
        for (int i = 0; i < cursor.getCount(); i++){
            cursor.moveToPosition(i);
            Log.i("COP CURSOR ", "at pos: " + String.valueOf(i) + " of value: " + String.valueOf(cursor.getInt(0)));
            Cop cop = copController.getCopByID(cursor.getInt(0));
            cops.add(cop);
        }

        copsAdapter = new ArrayAdapter<Cop>(getContext(), R.layout.support_simple_spinner_dropdown_item, cops);
        listView.setAdapter(copsAdapter);
    }

    @Override
    public void onCreateContextMenu(final ContextMenu menu, View v, final ContextMenu.ContextMenuInfo menuInfo) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
        menu.setHeaderTitle(copsAdapter.getItem(info.position).toString());
        menu.add("Edit");
        menu.getItem(0).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                return false;
            }
        });
        menu.add("delete");
        menu.getItem(1).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
                new Repository(getContext())
                        .getWritableDatabase()
                        .execSQL("DELETE FROM funkcjonariusz_has_sledztwo WHERE SLEDZTWO_ID = " + String.valueOf(probeId)
                                + " AND FUNKCJONARIUSZ_NR_ODZNAKI = " + String.valueOf(copsAdapter.getItem(info.position).id) + ";");
                refresh();
                return true;
            }
        });
    }

    @Override
    public void refresh(){
        fillListView(listView);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}