package com.kamilspisak.policedepartamentstorageapp.Probe;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.kamilspisak.policedepartamentstorageapp.Utils.Repository;
import com.kamilspisak.policedepartamentstorageapp.Interfaces.ICrud;
import com.kamilspisak.policedepartamentstorageapp.Utils.UserPrefs;

import java.util.ArrayList;

/**
 * Created by ksmal on 27.12.2015.
 */
public class ProbeController implements ICrud<Probe> {

    private final String KEY_TABLE_NAME = "Sledztwo";
    private final String KEY_ID = "_id";
    private final String KEY_NAME = "Nazwa";
    private final String KEY_DESCRIPTION = "Opis";
    private final String KEY_DEPARTAMENT_ID = "Komisariat_id";

    private Repository repository;
    private Context context;

    public ProbeController(Context context){
        this.context = context;
        repository = new Repository(context);
    }

    @Override
    public void create(Probe probe) {
        ContentValues probeContent = getContentValues(probe);
        repository.getWritableDatabase().insert(KEY_TABLE_NAME, null, probeContent);
    }

    private ContentValues getContentValues(Probe probe) {
        ContentValues probeContent = new ContentValues();
        probeContent.put(KEY_NAME, probe.name);
        probeContent.put(KEY_DESCRIPTION, probe.description);
        return probeContent;
    }

    @Override
    public void update(Probe probe) {
        ContentValues caseContent = getContentValues(probe);
        repository.getWritableDatabase().
                update(KEY_TABLE_NAME, caseContent, "WHERE " + KEY_ID + " = " + String.valueOf(probe.id) + ";", null);
    }

    @Override
    public void delete(Probe probe) {
        repository.getWritableDatabase()
                .execSQL("DELETE FROM" + KEY_TABLE_NAME + " WHERE " + KEY_ID + " = " + String.valueOf(probe.id) + ";");
    }

    @Override
    public ArrayList<Probe> read() {
        String departamentId = String.valueOf(new UserPrefs(context.getSharedPreferences(UserPrefs.PREFS_NAME, 0)).getDepId());

        ArrayList<Probe> probes = new ArrayList<Probe>();
        Cursor cursor = repository.getReadableDatabase()
                .rawQuery("SELECT * FROM " + KEY_TABLE_NAME + " WHERE " + KEY_DEPARTAMENT_ID + " = " + departamentId + ";", null);

        for (int i = 0; i < cursor.getCount(); i++){
            Probe probe = CursorToObject(cursor, i);
            Log.i("----------Cop ", probe.name + ", id: " + String.valueOf(probe.id));
            probes.add(probe);
        }

        return probes;
    }

    private Probe CursorToObject(Cursor cursor, int index){
        cursor.moveToPosition(index);
        Probe probe = new Probe(
                cursor.getInt(0),
                cursor.getString(1),
                cursor.getString(2)
                );
        return probe;
    }

    public Probe getProbeByID(int id){
        Cursor cursor = repository.getReadableDatabase()
                .rawQuery("SELECT * FROM " + KEY_TABLE_NAME + " WHERE " + KEY_ID + " = " + String.valueOf(id) + ";", null);
        return CursorToObject(cursor, 0);
    }
}
