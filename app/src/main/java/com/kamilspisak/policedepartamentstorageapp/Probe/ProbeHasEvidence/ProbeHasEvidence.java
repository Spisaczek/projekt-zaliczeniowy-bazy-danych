package com.kamilspisak.policedepartamentstorageapp.Probe.ProbeHasEvidence;

/**
 * Created by Kamil on 2016-01-06.
 */
public class ProbeHasEvidence {
    public int probeID;
    public int evidenceID;

    public ProbeHasEvidence(int probeID, int evidenceID) {
        this.probeID = probeID;
        this.evidenceID = evidenceID;
    }
}
