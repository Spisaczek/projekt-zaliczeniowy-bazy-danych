package com.kamilspisak.policedepartamentstorageapp.Probe.ProbeDetails;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.kamilspisak.policedepartamentstorageapp.Utils.Repository;
import com.kamilspisak.policedepartamentstorageapp.Evidence.Evidence;
import com.kamilspisak.policedepartamentstorageapp.Evidence.EvidenceController;
import com.kamilspisak.policedepartamentstorageapp.Interfaces.IRefreshableFragment;
import com.kamilspisak.policedepartamentstorageapp.R;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

public class EvidencesListFragment extends Fragment implements IRefreshableFragment{

    private static final String ARG_PROBE_ID = "PROBE_ID";

    private int probeId;
    private ArrayAdapter<Evidence> evidencesAdapter;
    private EvidenceController evidenceController;

    @Bind(R.id.listView) ListView listView;

    public EvidencesListFragment() {
        // Required empty public constructor
    }

    public static EvidencesListFragment newInstance(int probeId) {
        EvidencesListFragment fragment = new EvidencesListFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PROBE_ID, probeId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        probeId = getArguments().getInt(ARG_PROBE_ID);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_simple_list, container, false);
        ButterKnife.bind(this,view);
        fillListView();
        registerForContextMenu(listView);
        return view;
    }

    private void fillListView() {
        String query = "SELECT * FROM SLEDZTWO_has_DOWOD WHERE SLEDZTWO_ID = " + String.valueOf(probeId) + ";";
        Cursor cursor = new Repository(getContext())
                .getReadableDatabase()
                .rawQuery(query, null);

        evidenceController = new EvidenceController(getContext());
        ArrayList<Evidence> evidences = new ArrayList<>();
        for(int i = 0; i < cursor.getCount(); i++){
            cursor.moveToPosition(i);
            Evidence evidence = evidenceController.getEvidenceByID(cursor.getInt(1));
            evidences.add(evidence);
        }

        evidencesAdapter = new ArrayAdapter<Evidence>(getContext(), R.layout.support_simple_spinner_dropdown_item, evidences);
        listView.setAdapter(evidencesAdapter);
        listView.refreshDrawableState();
    }

    @Override
    public void onCreateContextMenu(final ContextMenu menu, View v, final ContextMenu.ContextMenuInfo menuInfo) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
        menu.setHeaderTitle(evidencesAdapter.getItem(info.position).toString());
        menu.add("Edit");
        menu.getItem(0).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                return false;
            }
        });
        menu.add("delete");
        menu.getItem(1).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
                new Repository(getContext())
                        .getWritableDatabase()
                        .execSQL("DELETE FROM SLEDZTWO_has_DOWOD WHERE SLEDZTWO_ID = " + String.valueOf(probeId)
                                + " AND DOWOD_id = " + String.valueOf(evidencesAdapter.getItem(info.position).id) + ";");
                refresh();
                return true;
            }
        });
    }

    @Override
    public void refresh(){
        fillListView();
    }

}
