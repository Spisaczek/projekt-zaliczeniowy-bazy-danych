package com.kamilspisak.policedepartamentstorageapp.Probe.CopHasProbe;

/**
 * Created by ksmal on 27.12.2015.
 */
public class CopHasProbe {

    public int copId;
    public int probeId;

    public CopHasProbe(int copId, int probeId) {
        this.copId = copId;
        this.probeId = probeId;
    }
}
