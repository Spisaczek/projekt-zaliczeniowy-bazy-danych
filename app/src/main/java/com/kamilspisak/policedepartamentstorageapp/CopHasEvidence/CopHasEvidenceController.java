package com.kamilspisak.policedepartamentstorageapp.CopHasEvidence;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.kamilspisak.policedepartamentstorageapp.Utils.Repository;
import com.kamilspisak.policedepartamentstorageapp.Interfaces.ICrud;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by Kamil on 2016-01-05.
 */
public class CopHasEvidenceController implements ICrud<CopHasEvidence> {
    private static String DATE_FORMAT = "yyyy.MM.dd 'at' HH:mm:ss";

    private final String KEY_TABLE_NAME = "FUNKCJONARIUSZ_has_DOWOD";
    private final String KEY_COP_ID = "FUNKCJONARIUSZ_NR_ODZNAKI";
    private final String KEY_EVIDENCE_ID = "DOWOD_id";
    private final String KEY_LEND_DATE = "Data_wypozyczenia";
    private final String KEY_RETURN_DATE = "Data_zwrotu";
    private Repository repository;

    public CopHasEvidenceController(Context context) {
        repository = new Repository(context);
    }

    private ContentValues getContentValues(CopHasEvidence copHasEvidence) {
        ContentValues copHasEvidenceContent = new ContentValues();
        copHasEvidenceContent.put(KEY_COP_ID, copHasEvidence.copId);
        copHasEvidenceContent.put(KEY_EVIDENCE_ID, copHasEvidence.evidenceId);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT);
        copHasEvidenceContent.put(KEY_LEND_DATE, simpleDateFormat.format(copHasEvidence.lendDate));
        if(copHasEvidence.returnDate != null)
            copHasEvidenceContent.put(KEY_RETURN_DATE, simpleDateFormat.format((copHasEvidence.returnDate)));
        return copHasEvidenceContent;
    }

    @Override
    public void create(CopHasEvidence copHasEvidence) {
        ContentValues copHasEvidenceContent = getContentValues(copHasEvidence);
        repository.getWritableDatabase().insert(KEY_TABLE_NAME, null, copHasEvidenceContent);
    }

    @Override
    public void update(CopHasEvidence copHasEvidence) {
        ContentValues caseContent = getContentValues(copHasEvidence);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT);
        String whereClause = KEY_COP_ID + " = " + String.valueOf(copHasEvidence.copId) +
                " AND " + KEY_EVIDENCE_ID + " = " + String.valueOf(copHasEvidence.evidenceId) + " AND " + KEY_LEND_DATE + " = '" + simpleDateFormat.format(copHasEvidence.lendDate) + "';";

        repository.getWritableDatabase().update(KEY_TABLE_NAME, caseContent, whereClause, null);
    }

    @Override
    public void delete(CopHasEvidence copHasEvidence) {
        throw new UnsupportedOperationException("Cannot delete this! It's permanent");
    }

    @Override
    public ArrayList<CopHasEvidence> read() {
        ArrayList<CopHasEvidence> copHasEvidences = new ArrayList<>();
        Cursor cursor = repository.getReadableDatabase()
                .rawQuery("SELECT * FROM " + KEY_TABLE_NAME + ";", null);

        for (int i = 0; i < cursor.getCount(); i++){
            CopHasEvidence copHasEvidence = CursorToObject(cursor, i);
            copHasEvidences.add(copHasEvidence);
        }

        return copHasEvidences;
    }

    private CopHasEvidence CursorToObject(Cursor cursor, int index){
        cursor.moveToPosition(index);

        CopHasEvidence copHasEvidence = new CopHasEvidence(
                cursor.getInt(0),
                cursor.getInt(1),
                Date.valueOf(cursor.getString(2)),
                Date.valueOf(cursor.getString(3))
        );

        return copHasEvidence;
    }
}
