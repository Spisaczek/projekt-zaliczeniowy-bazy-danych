package com.kamilspisak.policedepartamentstorageapp.CopHasEvidence;

import java.util.Date;

/**
 * Created by Kamil on 2016-01-05.
 */
public class CopHasEvidence {

    public int copId;
    public int evidenceId;
    Date lendDate;
    Date returnDate;

    public CopHasEvidence(int copId, int evidenceId, Date lendDate, Date returnDate) {
        this.copId = copId;
        this.evidenceId = evidenceId;
        this.lendDate = lendDate;
        this.returnDate = returnDate;
    }
}
