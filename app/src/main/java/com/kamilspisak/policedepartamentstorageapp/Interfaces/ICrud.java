package com.kamilspisak.policedepartamentstorageapp.Interfaces;

import java.util.ArrayList;

/**
 * Created by ksmal on 27.12.2015.
 */
public interface ICrud<T> {
    void create(T t);
    void update(T t);
    void delete(T t);
    ArrayList<T> read();
}
