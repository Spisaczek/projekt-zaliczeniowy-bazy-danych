package com.kamilspisak.policedepartamentstorageapp.Interfaces;

/**
 * Created by Kamil on 2016-01-07.
 */
public interface IRefreshableFragment {

    public void refresh();

}
