package com.kamilspisak.policedepartamentstorageapp.Utils;

import android.content.Context;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

/**
 * Created by ksmal on 23.12.2015.
 */
public class Repository extends SQLiteAssetHelper {

    private static final String DATABASE_NAME = "komisariaty.db3";
    private static final int DATABASE_VERSION = 1;

    public Repository(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
}
