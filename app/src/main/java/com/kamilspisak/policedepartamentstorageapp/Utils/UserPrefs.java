package com.kamilspisak.policedepartamentstorageapp.Utils;

import android.content.SharedPreferences;

/**
 * Created by ksmal on 16.01.2016.
 */
public class UserPrefs {

    public static final String PREFS_NAME = "DepPrefs";
    public static final String KEY_DEP_ID = "DepId";

    SharedPreferences settings;

    public UserPrefs(SharedPreferences settings) {
        this.settings = settings;
    }

    public void setDepId(int depId){
        settings.edit().putInt(KEY_DEP_ID, depId).commit();
    }

    public int getDepId(){
        int depId;
        depId = settings.getInt(KEY_DEP_ID, 0);
        return depId;
    }
}
