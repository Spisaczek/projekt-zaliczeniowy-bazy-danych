package com.kamilspisak.policedepartamentstorageapp.Evidence.EvidenceDetails;

import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.kamilspisak.policedepartamentstorageapp.Evidence.Evidence;
import com.kamilspisak.policedepartamentstorageapp.Evidence.EvidenceController;
import com.kamilspisak.policedepartamentstorageapp.R;
import com.kamilspisak.policedepartamentstorageapp.Utils.Repository;

import butterknife.Bind;
import butterknife.ButterKnife;

public class EvidenceMainFragment extends Fragment {

    private int evidenceId;
    private EvidenceController evidenceController;

    @Bind(R.id.image) ImageView imageView;
    @Bind(R.id.description) TextView descriptionText;
    @Bind(R.id.category) TextView categoryText;
    @Bind(R.id.status) TextView statusText;
    @Bind(R.id.added) TextView addedText;
    @Bind(R.id.comment) TextView commentText;
    @Bind(R.id.regal) TextView shellText;
    @Bind(R.id.polka) TextView shelliText;
    @Bind(R.id.box) TextView boxText;

    public EvidenceMainFragment() {
        // Required empty public constructor
    }

    public static EvidenceMainFragment newInstance(int id) {
        EvidenceMainFragment fragment = new EvidenceMainFragment();
        Bundle args = new Bundle();
        args.putInt("id", id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            evidenceId = getArguments().getInt("id");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_evidence_main, container, false);
        ButterKnife.bind(this, view);

        Evidence evidence = new EvidenceController(getContext()).getEvidenceByID(evidenceId);
        descriptionText.setText(evidence.description);
        categoryText.setText(evidence.category);
        statusText.setText(evidence.status);
        addedText.setText(evidence.addDate);
        commentText.setText(evidence.comment);
        if(evidence.photo!=null){
            imageView.setImageBitmap(BitmapFactory.decodeByteArray(evidence.getPhoto(),
                    0, evidence.photo.length));
        } else {
            Log.e("EvidenceView", "No photo data");
        }

        Repository repository = new Repository(getContext());
        Cursor cursor = repository.
                getReadableDatabase().
                rawQuery("SELECT Nr_regalu, Nr_polki, _id FROM POJEMNIK WHERE _id = (SELECT POJEMNIK_id FROM DOWOD WHERE _id = " + String.valueOf(evidenceId) + ");", null);
        cursor.moveToFirst();

        shellText.setText("Regał: " + String.valueOf(cursor.getInt(0)));
        shelliText.setText("Półka: " + String.valueOf(cursor.getInt(1)));
        boxText.setText("Pudełko: " + String.valueOf(cursor.getInt(2)));

        return view;
    }

}
