package com.kamilspisak.policedepartamentstorageapp.Evidence.EvidenceDetails;


import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.kamilspisak.policedepartamentstorageapp.Cop.Cop;
import com.kamilspisak.policedepartamentstorageapp.Cop.CopController;
import com.kamilspisak.policedepartamentstorageapp.R;
import com.kamilspisak.policedepartamentstorageapp.Utils.Repository;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

public class EvidenceLendHistoryFragment extends Fragment {
    private static String DATE_FORMAT = "yyyy.MM.dd 'at' HH:mm:ss";

    private static final String ARG_COP_ID = "COP_ID";

    private ArrayList<BorowedEvidence> evidences;
    private ArrayAdapter<BorowedEvidence> evidenceAdapter;
    private int evidenceId;

    @Bind(R.id.listView)
    ListView listView;

    public EvidenceLendHistoryFragment() {
        // Required empty public constructor
    }

    public static EvidenceLendHistoryFragment newInstance(int evidenceId) {
        EvidenceLendHistoryFragment fragment = new EvidenceLendHistoryFragment();
        Bundle args = new Bundle();
        args.putInt("id", evidenceId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            evidenceId = getArguments().getInt("id");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_simple_list, container, false);

        ButterKnife.bind(this, view);
        fillListView(listView);
        registerForContextMenu(listView);

        return view;
    }

    private void fillListView(ListView listView) {
        Cursor cursor = new Repository(getContext()).getReadableDatabase()
                .rawQuery("SELECT FUNKCJONARIUSZ_NR_ODZNAKI, DOWOD_id, Opis, Data_wypozyczenia, Data_zwrotu FROM funkcjonariusz_has_dowod JOIN DOWOD ON  funkcjonariusz_has_dowod.DOWOD_id = DOWOD._id WHERE DOWOD_id = " + String.valueOf(evidenceId) + ";", null);

        evidences = new ArrayList<>();
        for (int i = 0; i < cursor.getCount(); i++){
            cursor.moveToPosition(i);
            Log.i("EvidenceBorrowed CURSOR", "at pos: " + String.valueOf(i) + " of value: " + cursor.getString(0) + " return date = " + cursor.getString(4));
            BorowedEvidence borowedEvidence = new BorowedEvidence(cursor.getInt(0), cursor.getInt(1), cursor.getString(2), cursor.getString(3), cursor.getString(4));
            evidences.add(borowedEvidence);
        }

        evidenceAdapter = new BorowedEvidenceArrayAdapter(getContext(), evidences);
        listView.setAdapter(evidenceAdapter);
        listView.refreshDrawableState();
    }

    public class BorowedEvidenceArrayAdapter extends ArrayAdapter<BorowedEvidence>{

        public BorowedEvidenceArrayAdapter(Context context, ArrayList<BorowedEvidence> objects) {
            super(context, 0, objects);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            BorowedEvidence borowedEvidence = getItem(position);

            if(convertView == null){
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.evidence_item, parent, false);
            }

            TextView description = (TextView) convertView.findViewById(R.id.description);
            TextView addDate = (TextView) convertView.findViewById(R.id.lendDate);
            TextView returnDate = (TextView) convertView.findViewById(R.id.returnDate);

            Cop cop = new CopController(getContext()).getCopByID(borowedEvidence.copId);

            description.setText(cop.firstName + " " + cop.lastName);
            addDate.setText("From: " + borowedEvidence.lendDate);
            if(borowedEvidence.returnDate != null)
                returnDate.setText("To: " + borowedEvidence.returnDate);
            else returnDate.setText("To: Not returned yet");

            return convertView;
        }
    }

    public class BorowedEvidence{

        public int copId;
        public int evidenceId;
        public String description;
        public String lendDate;
        public String returnDate;

        public BorowedEvidence(int copId, int evidenceId, String description, String lendDate, String returnDate) {
            this.copId = copId;
            this.evidenceId = evidenceId;
            this.description = description;
            this.lendDate = lendDate;
            this.returnDate = returnDate;
        }

        @Override
        public String toString() {
            return description;
        }
    }

}
