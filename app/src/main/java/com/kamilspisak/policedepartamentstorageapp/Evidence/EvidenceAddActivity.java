package com.kamilspisak.policedepartamentstorageapp.Evidence;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.kamilspisak.policedepartamentstorageapp.R;
import com.kamilspisak.policedepartamentstorageapp.Utils.Repository;
import com.kamilspisak.policedepartamentstorageapp.Utils.UserPrefs;

import java.io.ByteArrayOutputStream;
import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EvidenceAddActivity extends AppCompatActivity {

    private static String DATE_FORMAT = "yyyy.MM.dd 'at' HH:mm:ss";
    private static final int REQUEST_IMAGE_CAPTURE = 1;

    private EvidenceController evidenceController;
    private Repository repository;
    private Bitmap imageBitmap;

    @Bind(R.id.description) EditText descriptionText;
    @Bind(R.id.category) EditText categoryText;
    @Bind(R.id.status) EditText statusText;
    @Bind(R.id.added) EditText addedText;
    @Bind(R.id.comment) EditText commentText;
    @Bind(R.id.add_button) Button addButton;
    @Bind(R.id.spinner)Spinner spinner;
    @Bind(R.id.make_photo_btn) Button photoButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_evidence_view);
        ButterKnife.bind(this);
        evidenceController = new EvidenceController(this);
        repository = new Repository(this);

        int depId = new UserPrefs(getSharedPreferences(UserPrefs.PREFS_NAME,0)).getDepId();
        Cursor cursor = repository.getReadableDatabase().rawQuery("SELECT _id FROM POJEMNIK WHERE MAGAZYN_KOMISARIAT_id = " + String.valueOf(depId) + ";", null);
        cursor.moveToFirst();
        ArrayList<String> boxes = new ArrayList<>();
        for (int i = 0; i < cursor.getCount(); i++){
            cursor.moveToPosition(i);
            boxes.add(String.valueOf(cursor.getInt(0)));
        }

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, boxes);
        spinner.setAdapter(arrayAdapter);

        statusText.setText("Dostepny");

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT);
        Date nowDate = new Date(Calendar.getInstance().getTime().getTime());
        addedText.setText(simpleDateFormat.format(nowDate));

    }

    @OnClick(R.id.add_button)
    void onAddButtonClicked(){
        Evidence evidence = getEvidence();
        addEviToRepo(evidence);
        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        finish();
    }

    @OnClick(R.id.make_photo_btn)
    void onMakePhotoBtnClicker(){
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            imageBitmap = (Bitmap) extras.get("data");
            Toast.makeText(this, "Photo submited!", Toast.LENGTH_SHORT);
        }
    }

    private Evidence getEvidence(){
        String string = spinner.getItemAtPosition(spinner.getSelectedItemPosition()).toString();
        int boxId = Integer.valueOf(string);
        Evidence evidence = new Evidence(0,
                boxId,
                descriptionText.getText().toString(),
                categoryText.getText().toString(),
                statusText.getText().toString(),
                addedText.getText().toString(),
                commentText.getText().toString()
        );

        if(imageBitmap != null){
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            imageBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] byteArray = stream.toByteArray();
            evidence.setPhoto(byteArray);
        } else {
            Log.e("EvidenceAdd", "No imageBitmap data");
        }

        return evidence;
    }

    private void addEviToRepo(Evidence evidence){
        evidenceController.create(evidence);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
    }

}
