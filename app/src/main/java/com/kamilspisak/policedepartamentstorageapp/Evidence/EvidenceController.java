package com.kamilspisak.policedepartamentstorageapp.Evidence;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.kamilspisak.policedepartamentstorageapp.Interfaces.ICrud;
import com.kamilspisak.policedepartamentstorageapp.Utils.Repository;
import com.kamilspisak.policedepartamentstorageapp.Utils.UserPrefs;

import java.util.ArrayList;

/**
 * Created by ksmal on 23.12.2015.
 */
public class EvidenceController implements ICrud<Evidence> {

    private static final String KEY_ID = "id";
    private static final String KEY_POJEMNIK_ID = "POJEMNIK_id";
    private static final String KEY_DESCRIPTION = "Opis";
    private static final String KEY_CATEGORY = "Kategoria";
    private static final String KEY_STATUS = "Status_2";
    private static final String KEY_ADD_DATE = "Data_dodania";
    private static final String KEY_COMMENT = "Komentarz";
    private static final String KEY_PHOTO = "Fotografia";

    private Context context;
    private Repository repository;


    public EvidenceController(Context context) {
        this.context = context;
        repository = new Repository(context);
    }

    public Evidence getEvidenceByID(int id){
        Cursor cursor = repository.getReadableDatabase().rawQuery("SELECT * FROM dowod WHERE _id=" + String.valueOf(id) + ";", null);
        return cursorToObject(cursor, 0);
    }

    public byte[] getEvidencePhotoByID(int id){
        Cursor cursor = repository.getReadableDatabase().rawQuery("SELECT * FROM dowod WHERE _id=" + String.valueOf(id) + ";", null);
        return cursor.getBlob(7);
    }

    private Evidence cursorToObject(Cursor cursor, int index){
        cursor.moveToPosition(index);
        Evidence evidence = new Evidence(
                cursor.getInt(0),
                cursor.getInt(1),
                cursor.getString(2),
                cursor.getString(3),
                cursor.getString(4),
                cursor.getString(5),
                cursor.getString(6));
        try{
            evidence.setPhoto(cursor.getBlob(7));
        } catch (Exception e){
            Log.e("FOTO", "BRAK FOTO");
        }
        return evidence;
    }

    @Override
    public void create(Evidence evidence) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_POJEMNIK_ID, evidence.boxId);
        contentValues.put(KEY_DESCRIPTION, evidence.description);
        contentValues.put(KEY_CATEGORY, evidence.category);
        contentValues.put(KEY_STATUS, evidence.status);
        contentValues.put(KEY_ADD_DATE, evidence.addDate);
        contentValues.put(KEY_COMMENT, evidence.comment);
        if(evidence.photo != null){
            contentValues.put(KEY_PHOTO, evidence.photo);
        } else {
            Log.e("EvidenceCreate", "No photo data");
        }
        repository.getWritableDatabase().insert("dowod", null, contentValues);
    }

    @Override
    public void update(Evidence evidence) {
        //TODO: Fill this by logic
    }

    @Override
    public void delete(Evidence evidence) {
        repository.getWritableDatabase().execSQL("DELETE FROM dowod WHERE _id = " + String.valueOf(evidence.id) + ";");
    }

    @Override
    public ArrayList<Evidence> read() {
        String departamentId = String.valueOf(new UserPrefs(context.getSharedPreferences(UserPrefs.PREFS_NAME, 0)).getDepId());
        String getAllMagazines = "SELECT _id FROM MAGAZYN WHERE KOMISARIAT_id = " + departamentId;
        String getAllBoxes = "(SELECT _id FROM POJEMNIK WHERE MAGAZYN_KOMISARIAT_id = " + departamentId + ")";
        ArrayList<Evidence> evidences = new ArrayList<>();

        Cursor cursor = repository.getReadableDatabase().rawQuery("SELECT * FROM dowod WHERE POJEMNIK_id IN " + getAllBoxes + ";", null);
        for (int i = 0; i < cursor.getCount(); i++){
            Evidence evidence = cursorToObject(cursor, i);
            Log.i("----------Evidence ", evidence.description + ", id: " + String.valueOf(evidence.id));
            evidences.add(evidence);
        }

        return evidences;
    }
}
