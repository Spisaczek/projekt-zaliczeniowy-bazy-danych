package com.kamilspisak.policedepartamentstorageapp.Evidence;

import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.kamilspisak.policedepartamentstorageapp.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public class EvidenceViewActivity extends AppCompatActivity {

    private Evidence evidence;

    @Bind(R.id.image) ImageView imageView;
    @Bind(R.id.description) TextView descriptionText;
    @Bind(R.id.category) TextView categoryText;
    @Bind(R.id.status) TextView statusText;
    @Bind(R.id.added) TextView addedText;
    @Bind(R.id.comment) TextView commentText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_evidence_details);
        ButterKnife.bind(this);

        int id = getIntent().getIntExtra("id", 1);
        evidence = new EvidenceController(this).getEvidenceByID(id);
        descriptionText.setText(evidence.description);
        categoryText.setText(evidence.category);
        statusText.setText(evidence.status);
        addedText.setText(evidence.addDate);
        commentText.setText(evidence.comment);
        imageView.setImageResource(R.drawable.polish_police_logo);
        if(evidence.photo!=null){
            imageView.setImageBitmap(BitmapFactory.decodeByteArray(evidence.getPhoto(),
                    0, evidence.photo.length));
        } else {
            Log.e("EvidenceView", "No photo data");
        }
    }

}
