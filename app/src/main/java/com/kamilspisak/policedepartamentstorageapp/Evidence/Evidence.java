package com.kamilspisak.policedepartamentstorageapp.Evidence;

/**
 * Created by ksmal on 23.12.2015.
 */
public class Evidence {

    public int id;
    public int boxId;
    public String description;
    public String category;
    public String status;
    public String addDate;
    public String comment;
    public byte[] photo;

    public Evidence(int id, int boxId, String description, String category, String status, String addDate, String comment) {
        this.id = id;
        this.boxId = boxId;
        this.description = description;
        this.category = category;
        this.status = status;
        this.addDate = addDate;
        this.comment = comment;
    }

    public void setPhoto(byte[] bytes){
        this.photo = bytes;
    }

    public byte[] getPhoto(){
        return photo;
    }

    @Override
    public String toString() {
        return description;
    }
}
