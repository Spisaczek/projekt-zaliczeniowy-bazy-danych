package com.kamilspisak.policedepartamentstorageapp.Evidence;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.kamilspisak.policedepartamentstorageapp.Cop.CopsListActivity;
import com.kamilspisak.policedepartamentstorageapp.Departament.DepartamentList;
import com.kamilspisak.policedepartamentstorageapp.Evidence.EvidenceDetails.EvidenceViewTabActivity;
import com.kamilspisak.policedepartamentstorageapp.Probe.ProbesListActivity;
import com.kamilspisak.policedepartamentstorageapp.R;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.SectionDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.software.shell.fab.ActionButton;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

public class EvidencesListActivity extends AppCompatActivity {

    private Drawer drawer;
    private EvidenceController evidenceController;
    private ArrayAdapter<Evidence> adapter;
    private ActionButton actionButton;
    private ArrayList<Evidence> evidences;

    @Bind(R.id.listView) ListView listView;
    @Bind(R.id.fab) FloatingActionButton fab;

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simple_list_with_action_button);
        ButterKnife.bind(this);

        evidenceController = new EvidenceController(this);
        evidences = evidenceController.read();

        adapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, evidences);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent myIntent = new Intent(EvidencesListActivity.this, EvidenceViewTabActivity.class);
                myIntent.putExtra("id", evidences.get(position).id);
                EvidencesListActivity.this.startActivity(myIntent);
            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(EvidencesListActivity.this, EvidenceAddActivity.class);
                EvidencesListActivity.this.startActivity(myIntent);
            }
        });

        registerForContextMenu(listView);
        buildDrawer();
    }

    private void buildDrawer() {
        PrimaryDrawerItem departamentItem = new PrimaryDrawerItem().withName("Departaments").withIcon(FontAwesome.Icon.faw_building);
        PrimaryDrawerItem copsItem = new PrimaryDrawerItem().withName("Cops").withIcon(FontAwesome.Icon.faw_male);
        PrimaryDrawerItem probesItem = new PrimaryDrawerItem().withName("Probes").withIcon(FontAwesome.Icon.faw_suitcase);
        PrimaryDrawerItem evidencesItem = new PrimaryDrawerItem().withName("Evidences").withIcon(FontAwesome.Icon.faw_archive);

        AccountHeader headerResult = new AccountHeaderBuilder()
                .withActivity(this)
                .withHeaderBackground(R.drawable.polish_police_logo)
                .build();

        drawer = new DrawerBuilder()
                .withActivity(this)
                .withAccountHeader(headerResult)
                .withTranslucentStatusBar(false)
                .withActionBarDrawerToggle(true)
                .withActionBarDrawerToggleAnimated(true)
                .addDrawerItems(
                        departamentItem,
                        copsItem,
                        probesItem,
                        evidencesItem,
                        new SectionDrawerItem().withName("Other"),
                        new PrimaryDrawerItem().withName("Author").withIcon(FontAwesome.Icon.faw_info)
                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        Intent intent;
                        switch (position) {
                            case 1:
                                drawer.closeDrawer();
                                intent = new Intent(EvidencesListActivity.this, DepartamentList.class);
                                EvidencesListActivity.this.startActivity(intent);
                                finish();
                                break;
                            case 2:
                                drawer.closeDrawer();
                                intent = new Intent(EvidencesListActivity.this, CopsListActivity.class);
                                EvidencesListActivity.this.startActivity(intent);
                                finish();
                                break;
                            case 3:
                                drawer.closeDrawer();
                                intent = new Intent(EvidencesListActivity.this, ProbesListActivity.class);
                                EvidencesListActivity.this.startActivity(intent);
                                finish();
                                break;
                            case 4:
                                drawer.closeDrawer();
                                break;
                        }
                        return true;
                    }
                })
                .withSelectedItem(-1)
                .build();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void onCreateContextMenu(final ContextMenu menu, View v, final ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
        menu.setHeaderTitle(adapter.getItem(info.position).toString());
        menu.add("Edit");
        menu.getItem(0).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                return false;
            }
        });
        menu.add("delete");
        menu.getItem(1).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
                int index = info.position;
                evidenceController.delete(evidences.get(index));
                return true;
            }
        });
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) refreshListView();
    }

    private void refreshListView(){
        evidences = evidenceController.read();
        adapter = new ArrayAdapter<Evidence>(this, R.layout.support_simple_spinner_dropdown_item, evidences);
        listView.setAdapter(adapter);
        listView.refreshDrawableState();
    }

    @Override
    public void onBackPressed() {
        if (drawer != null && drawer.isDrawerOpen()) {
            drawer.closeDrawer();
        } else {
            super.onBackPressed();
        }
    }
}

