package com.kamilspisak.policedepartamentstorageapp.Cop;

/**
 * Created by ksmal on 27.12.2015.
 */


public class Cop {

    public int id;
    public int departamentId;
    public String firstName;
    public String lastName;
    public String rank;

    public Cop(int id, int departamentId, String firstName, String lastName, String rank){
        this.id = id;
        this.departamentId = departamentId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.rank = rank;
    }

    @Override
    public String toString() {
        return firstName + " " + lastName;
    }
}
