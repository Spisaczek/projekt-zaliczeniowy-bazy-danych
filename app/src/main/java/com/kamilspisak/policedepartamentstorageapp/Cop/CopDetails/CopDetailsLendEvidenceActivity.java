package com.kamilspisak.policedepartamentstorageapp.Cop.CopDetails;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.kamilspisak.policedepartamentstorageapp.CopHasEvidence.CopHasEvidence;
import com.kamilspisak.policedepartamentstorageapp.CopHasEvidence.CopHasEvidenceController;
import com.kamilspisak.policedepartamentstorageapp.Utils.Repository;
import com.kamilspisak.policedepartamentstorageapp.Evidence.Evidence;
import com.kamilspisak.policedepartamentstorageapp.Evidence.EvidenceController;
import com.kamilspisak.policedepartamentstorageapp.R;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;

public class CopDetailsLendEvidenceActivity extends AppCompatActivity {
    private static String DATE_FORMAT = "yyyy.MM.dd 'at' HH:mm:ss";

    private EvidenceController evidenceController;
    private ArrayList<Evidence> evidences;
    private ArrayAdapter<Evidence> evidencesAdapter;
    private int copId;

    @Bind(R.id.listView) ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simple_list);
        ButterKnife.bind(this);

        copId = getIntent().getIntExtra("id",0);

        fillListView(listView);
    }

    private void fillListView(ListView listView) {
        String getAllProbes = "(SELECT SLEDZTWO_ID FROM FUNKCJONARIUSZ_has_SLEDZTWO WHERE FUNKCJONARIUSZ_NR_ODZNAKI = " + String.valueOf(copId) + ")";
        String getAllEvidences = "SELECT DOWOD_id FROM SLEDZTWO_has_DOWOD JOIN DOWOD ON SLEDZTWO_has_DOWOD.DOWOD_id = DOWOD._id WHERE SLEDZTWO_ID IN " + getAllProbes + " AND DOWOD.Status_2 LIKE 'Dostepny';";
        Cursor cursor = new Repository(this)
                .getReadableDatabase()
                .rawQuery(getAllEvidences, null);

        evidenceController = new EvidenceController(this);
        evidences = new ArrayList<>();
        for(int i = 0; i < cursor.getCount(); i++){
            cursor.moveToPosition(i);
            Evidence evidence = evidenceController.getEvidenceByID(cursor.getInt(0));
            evidences.add(evidence);
        }

        evidencesAdapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, evidences);
        listView.setAdapter(evidencesAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Date nowDate = new Date(Calendar.getInstance().getTime().getTime());

                CopHasEvidence copHasEvidence = new CopHasEvidence(copId, evidences.get(position).id, nowDate, null);
                new CopHasEvidenceController(getBaseContext()).create(copHasEvidence);
                new Repository(getBaseContext()).getWritableDatabase().execSQL("UPDATE DOWOD SET Status_2 = 'Wypozyczony " + copHasEvidence.copId + "' WHERE _id = " + String.valueOf(copHasEvidence.evidenceId) + ";");
                finish();
            }
        });
    }

}
