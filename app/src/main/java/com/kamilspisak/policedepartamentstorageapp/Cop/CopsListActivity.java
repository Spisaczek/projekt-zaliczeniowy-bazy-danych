package com.kamilspisak.policedepartamentstorageapp.Cop;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.kamilspisak.policedepartamentstorageapp.Cop.CopDetails.CopViewTabActivity;
import com.kamilspisak.policedepartamentstorageapp.Departament.DepartamentList;
import com.kamilspisak.policedepartamentstorageapp.Evidence.EvidencesListActivity;
import com.kamilspisak.policedepartamentstorageapp.Probe.ProbesListActivity;
import com.kamilspisak.policedepartamentstorageapp.R;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.SectionDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CopsListActivity extends AppCompatActivity {

    private Drawer drawer;
    private CopController copController;
    private ArrayList<Cop> cops;

    @Bind(R.id.listView) ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simple_list_with_action_button);
        ButterKnife.bind(this);

        copController = new CopController(this);
        cops = copController.read();

        ArrayAdapter<Cop> adapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, cops);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent myIntent = new Intent(CopsListActivity.this, CopViewTabActivity.class);
                myIntent.putExtra("id", cops.get(position).id);
                CopsListActivity.this.startActivity(myIntent);
            }
        });
        buildDrawer();
    }

    private void buildDrawer() {
        PrimaryDrawerItem departamentItem = new PrimaryDrawerItem().withName("Departaments").withIcon(FontAwesome.Icon.faw_building);
        PrimaryDrawerItem copsItem = new PrimaryDrawerItem().withName("Cops").withIcon(FontAwesome.Icon.faw_male);
        PrimaryDrawerItem probesItem = new PrimaryDrawerItem().withName("Probes").withIcon(FontAwesome.Icon.faw_suitcase);
        PrimaryDrawerItem evidencesItem = new PrimaryDrawerItem().withName("Evidences").withIcon(FontAwesome.Icon.faw_archive);

        AccountHeader headerResult = new AccountHeaderBuilder()
                .withActivity(this)
                .withHeaderBackground(R.drawable.polish_police_logo)
                .build();

        drawer = new DrawerBuilder()
                .withActivity(this)
                .withAccountHeader(headerResult)
                .withTranslucentStatusBar(false)
                .withActionBarDrawerToggle(true)
                .withActionBarDrawerToggleAnimated(true)
                .addDrawerItems(
                        departamentItem,
                        copsItem,
                        probesItem,
                        evidencesItem,
                        new SectionDrawerItem().withName("Other"),
                        new PrimaryDrawerItem().withName("Author").withIcon(FontAwesome.Icon.faw_info)
                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        Intent intent;
                        switch (position) {
                            case 1:
                                drawer.closeDrawer();
                                intent = new Intent(CopsListActivity.this, DepartamentList.class);
                                CopsListActivity.this.startActivity(intent);
                                finish();
                                break;
                            case 2:
                                drawer.closeDrawer();
                                break;
                            case 3:
                                drawer.closeDrawer();
                                intent = new Intent(CopsListActivity.this, ProbesListActivity.class);
                                CopsListActivity.this.startActivity(intent);
                                finish();
                                break;
                            case 4:
                                drawer.closeDrawer();
                                intent = new Intent(CopsListActivity.this, EvidencesListActivity.class);
                                CopsListActivity.this.startActivity(intent);
                                finish();
                                break;
                        }
                        return true;
                    }
                })
                .withSelectedItem(-1)
                .build();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @OnClick(R.id.fab)
    void OnFabClicked(){
        Intent intent = new Intent(CopsListActivity.this, CopAddActivity.class);
        CopsListActivity.this.startActivity(intent);
    }
}
