package com.kamilspisak.policedepartamentstorageapp.Cop.CopDetails;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kamilspisak.policedepartamentstorageapp.Cop.Cop;
import com.kamilspisak.policedepartamentstorageapp.Cop.CopController;
import com.kamilspisak.policedepartamentstorageapp.R;

import butterknife.Bind;
import butterknife.ButterKnife;


public class CopDetailsMainFragment extends Fragment {

    private static final String ARG_COP_ID = "COP_ID";

    private CopController copController;
    private int copId;

    @Bind(R.id.first_name_textView) TextView firstName;
    @Bind(R.id.last_name_textView) TextView lastName;
    @Bind(R.id.rank_textView) TextView rank;

    public CopDetailsMainFragment() {
        // Required empty public constructor
    }

    public static CopDetailsMainFragment newInstance(int copId) {
        CopDetailsMainFragment fragment = new CopDetailsMainFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COP_ID, copId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        copId = getArguments().getInt(ARG_COP_ID);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_cop_details_main, container, false);
        ButterKnife.bind(this, view);

        CopController copController = new CopController(getContext());
        Cop cop = copController.getCopByID(copId);

        firstName.setText(cop.firstName);
        lastName.setText(cop.lastName);
        rank.setText(cop.rank);

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
