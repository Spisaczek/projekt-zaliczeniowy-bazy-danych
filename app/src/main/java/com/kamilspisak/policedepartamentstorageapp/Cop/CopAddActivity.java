package com.kamilspisak.policedepartamentstorageapp.Cop;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.kamilspisak.policedepartamentstorageapp.R;
import com.kamilspisak.policedepartamentstorageapp.Utils.UserPrefs;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CopAddActivity extends AppCompatActivity {

    @Bind(R.id.nameText)
    EditText nameText;
    @Bind(R.id.lastNameText)
    EditText lastNameText;
    @Bind(R.id.submitBtn)
    Button submitBtn;
    @Bind(R.id.spinner)
    Spinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cop_add);
        ButterKnife.bind(this);

        ArrayList<String> ranks = new ArrayList<>();
        ranks.add("Posterunkowy");
        ranks.add("Starszy Posterunkowy");
        ranks.add("Sierżant");
        ranks.add("Starszy Sierżant");
        ranks.add("Sierżant Sztabowy");
        ranks.add("Młodszy Aspirant");
        ranks.add("Aspirant");
        ranks.add("Starszy Aspirant");
        ranks.add("Podkomisarz");
        ranks.add("Komisarz");
        ranks.add("Nadkomisarz");

        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, ranks);
        spinner.setAdapter(spinnerAdapter);
    }

    @OnClick(R.id.submitBtn)
    void OnSubmitBtnClick(){
        int departamentId = new UserPrefs(getSharedPreferences(UserPrefs.PREFS_NAME,0)).getDepId();
        Cop cop = new Cop(0, departamentId, nameText.getText().toString(), lastNameText.getText().toString(), spinner.getSelectedItem().toString());
        new CopController(this).create(cop);
        finish();
    }
}
