package com.kamilspisak.policedepartamentstorageapp.Cop.CopDetails;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.kamilspisak.policedepartamentstorageapp.CopHasEvidence.CopHasEvidence;
import com.kamilspisak.policedepartamentstorageapp.CopHasEvidence.CopHasEvidenceController;
import com.kamilspisak.policedepartamentstorageapp.Utils.Repository;
import com.kamilspisak.policedepartamentstorageapp.Evidence.Evidence;
import com.kamilspisak.policedepartamentstorageapp.Evidence.EvidenceController;
import com.kamilspisak.policedepartamentstorageapp.Interfaces.IRefreshableFragment;
import com.kamilspisak.policedepartamentstorageapp.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

public class CopDetailsEvidencesListFragment extends Fragment implements IRefreshableFragment {

    private static String DATE_FORMAT = "yyyy.MM.dd 'at' HH:mm:ss";

    private static final String ARG_COP_ID = "COP_ID";

    private ArrayList<BorowedEvidence> evidences;
    private ArrayAdapter<BorowedEvidence> evidenceAdapter;
    private int copId;

    @Bind(R.id.listView)
    ListView listView;

    public CopDetailsEvidencesListFragment() {
        // Required empty public constructor
    }

    public static CopDetailsEvidencesListFragment newInstance(int copId) {
        CopDetailsEvidencesListFragment fragment = new CopDetailsEvidencesListFragment();

        Bundle args = new Bundle();
        args.putInt(ARG_COP_ID, copId);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        copId = getArguments().getInt(ARG_COP_ID);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_simple_list, container, false);

        ButterKnife.bind(this, view);
        fillListView(listView);
        registerForContextMenu(listView);

        return view;
    }

    @Override
    public void onCreateContextMenu(final ContextMenu menu, View v, final ContextMenu.ContextMenuInfo menuInfo) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
        menu.setHeaderTitle(evidenceAdapter.getItem(info.position).toString());

        menu.add("Return");
        menu.getItem(0).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

                SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
                Date nowDate = new Date(Calendar.getInstance().getTime().getTime());

                BorowedEvidence borowedEvidence = evidenceAdapter.getItem(info.position);

                Date lendParsed = null;
                try {
                    lendParsed = dateFormat.parse(borowedEvidence.lendDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                new CopHasEvidenceController(getContext()).update(new CopHasEvidence(borowedEvidence.copId, borowedEvidence.evidenceId, lendParsed, nowDate));
                new Repository(getContext()).getWritableDatabase().execSQL("UPDATE DOWOD SET Status_2 = 'Dostepny' WHERE _id = " + borowedEvidence.evidenceId + ";");
                refresh();
                return true;
            }
        });
    }

    private void fillListView(ListView listView) {
        Cursor cursor = new Repository(getContext()).getReadableDatabase()
                .rawQuery("SELECT FUNKCJONARIUSZ_NR_ODZNAKI, DOWOD_id, Opis, Data_wypozyczenia, Data_zwrotu FROM funkcjonariusz_has_dowod JOIN DOWOD ON  funkcjonariusz_has_dowod.DOWOD_id = DOWOD._id WHERE FUNKCJONARIUSZ_NR_ODZNAKI = " + String.valueOf(copId) + ";", null);

        evidences = new ArrayList<>();
        for (int i = 0; i < cursor.getCount(); i++){
            cursor.moveToPosition(i);
            Log.i("EvidenceBorrowed CURSOR", "at pos: " + String.valueOf(i) + " of value: " + cursor.getString(0) + " return date = " + cursor.getString(4));
            BorowedEvidence borowedEvidence = new BorowedEvidence(cursor.getInt(0), cursor.getInt(1), cursor.getString(2), cursor.getString(3), cursor.getString(4));
            evidences.add(borowedEvidence);
        }

        evidenceAdapter = new BorowedEvidenceArrayAdapter(getContext(), evidences);
        listView.setAdapter(evidenceAdapter);
        listView.refreshDrawableState();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void refresh() {
        fillListView(listView);
    }

    public class BorowedEvidenceArrayAdapter extends ArrayAdapter<BorowedEvidence>{

        public BorowedEvidenceArrayAdapter(Context context, ArrayList<BorowedEvidence> objects) {
            super(context, 0, objects);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            BorowedEvidence borowedEvidence = getItem(position);

            if(convertView == null){
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.evidence_item, parent, false);
            }

            TextView description = (TextView) convertView.findViewById(R.id.description);
            TextView addDate = (TextView) convertView.findViewById(R.id.lendDate);
            TextView returnDate = (TextView) convertView.findViewById(R.id.returnDate);

            description.setText(borowedEvidence.description);
            addDate.setText("From: " + borowedEvidence.lendDate);
            if(borowedEvidence.returnDate != null)
                returnDate.setText("To: " + borowedEvidence.returnDate);
            else returnDate.setText("To: Not returned yet");

            return convertView;
        }
    }

    public class BorowedEvidence{

        public int copId;
        public int evidenceId;
        public String description;
        public String lendDate;
        public String returnDate;

        public BorowedEvidence(int copId, int evidenceId, String description, String lendDate, String returnDate) {
            this.copId = copId;
            this.evidenceId = evidenceId;
            this.description = description;
            this.lendDate = lendDate;
            this.returnDate = returnDate;
        }

        @Override
        public String toString() {
            return description;
        }
    }
}
