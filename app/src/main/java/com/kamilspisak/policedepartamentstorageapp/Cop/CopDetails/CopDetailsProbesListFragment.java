package com.kamilspisak.policedepartamentstorageapp.Cop.CopDetails;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.kamilspisak.policedepartamentstorageapp.Utils.Repository;
import com.kamilspisak.policedepartamentstorageapp.Probe.Probe;
import com.kamilspisak.policedepartamentstorageapp.Probe.ProbeController;
import com.kamilspisak.policedepartamentstorageapp.R;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

public class CopDetailsProbesListFragment extends Fragment {

    private static final String ARG_COP_ID = "COP_ID";

    private ArrayAdapter<Probe> probesAdapter;
    private int copId;

    @Bind(R.id.listView) ListView listView;

    public CopDetailsProbesListFragment() {
        // Required empty public constructor
    }

    public static CopDetailsProbesListFragment newInstance(int copId) {
        CopDetailsProbesListFragment fragment = new CopDetailsProbesListFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COP_ID, copId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        copId = getArguments().getInt(ARG_COP_ID);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_simple_list, container, false);
        ButterKnife.bind(this, view);

        registerForContextMenu(listView);
        fillListView(listView);

        return view;
    }

    private void fillListView(ListView listView) {
        String query = "SELECT * FROM funkcjonariusz_has_sledztwo WHERE FUNKCJONARIUSZ_NR_ODZNAKI = " + String.valueOf(copId) + ";";
        Cursor cursor = new Repository(getContext()).getReadableDatabase()
                .rawQuery(query, null);

        ProbeController probeController = new ProbeController(getContext());
        ArrayList<Probe> probes = new ArrayList<>();
        for (int i = 0; i < cursor.getCount(); i++){
            cursor.moveToPosition(i);
            Log.i("Probe CURSOR ", "at pos: " + String.valueOf(i) + " of value: " + String.valueOf(cursor.getInt(0)));
            Probe probe = probeController.getProbeByID(cursor.getInt(1));
            probes.add(probe);
        }

        probesAdapter = new ArrayAdapter<>(getContext(), R.layout.support_simple_spinner_dropdown_item, probes);
        listView.setAdapter(probesAdapter);
    }

    @Override
    public void onCreateContextMenu(final ContextMenu menu, View v, final ContextMenu.ContextMenuInfo menuInfo) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
        menu.setHeaderTitle(probesAdapter.getItem(info.position).toString());
        menu.add("Edit");
        menu.getItem(0).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                return false;
            }
        });
        menu.add("delete");
        menu.getItem(1).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
                //TODO: Add delete option
                /*
                new Repository(getContext())
                        .getWritableDatabase()
                        .execSQL("DELETE FROM funkcjonariusz_has_sledztwo WHERE SLEDZTWO_ID = " + String.valueOf(probeId)
                                + " AND FUNKCJONARIUSZ_NR_ODZNAKI = " + String.valueOf(copsAdapter.getItem(info.position).id) + ";");
                refresh();
                */
                return true;
            }
        });
    }

}
