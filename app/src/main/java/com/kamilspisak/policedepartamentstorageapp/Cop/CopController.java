package com.kamilspisak.policedepartamentstorageapp.Cop;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.util.Log;

import com.kamilspisak.policedepartamentstorageapp.Utils.Repository;
import com.kamilspisak.policedepartamentstorageapp.Interfaces.ICrud;
import com.kamilspisak.policedepartamentstorageapp.Utils.UserPrefs;

import java.util.ArrayList;

/**
 * Created by ksmal on 27.12.2015.
 */

public class CopController implements ICrud<Cop> {

    private final String KEY_TABLE_NAME = "Funkcjonariusz";
    private final String KEY_ID = "_id";
    private final String KEY_DEPARTAMENT_ID = "KOMISARIAT_id";
    private final String KEY_FIRST_NAME = "Imie";
    private final String KEY_LAST_NAME = "Nazwisko";
    private final String KEY_RANK = "Stopien";

    private Context context;
    private Repository repository;

    public CopController(Context context){
        this.context = context;
        repository = new Repository(context);
    }

    public void create(Cop cop){
        ContentValues copContent = getContentValues(cop);
        repository.getWritableDatabase().insert(KEY_TABLE_NAME, null, copContent);
    }

    @NonNull
    private ContentValues getContentValues(Cop cop) {
        ContentValues copContent = new ContentValues();
        copContent.put(KEY_DEPARTAMENT_ID, cop.departamentId);
        copContent.put(KEY_FIRST_NAME, cop.firstName);
        copContent.put(KEY_LAST_NAME, cop.lastName);
        copContent.put(KEY_RANK, cop.rank);
        return copContent;
    }

    public void update(Cop cop){
        ContentValues copContent = getContentValues(cop);
        repository.getWritableDatabase().
                update(KEY_TABLE_NAME, copContent, "WHERE " + KEY_ID + " = " + String.valueOf(cop.id) + ";", null);
    }

    public void delete(Cop cop){
        repository.getWritableDatabase()
                .execSQL("DELETE FROM" + KEY_TABLE_NAME + " WHERE " + KEY_ID + " = " + String.valueOf(cop.id) + ";");
    }

    public ArrayList<Cop> read(){
        String departamentId = String.valueOf(new UserPrefs(context.getSharedPreferences(UserPrefs.PREFS_NAME, 0)).getDepId());

        ArrayList<Cop> cops = new ArrayList<Cop>();
        Cursor cursor = repository.getReadableDatabase()
                .rawQuery("SELECT * FROM " + KEY_TABLE_NAME + " WHERE KOMISARIAT_id = " + departamentId + ";", null);

        for (int i = 0; i < cursor.getCount(); i++){
            Cop cop = cursorToObject(cursor, i);
            Log.i("----------Cop ", cop.firstName + " " + cop.lastName + ", id: " + String.valueOf(cop.id));
            cops.add(cop);
        }

        return cops;
    }

    private Cop cursorToObject(Cursor cursor, int index){
        cursor.moveToPosition(index);
        Cop cop = new Cop(
                cursor.getInt(0),
                cursor.getInt(1),
                cursor.getString(2),
                cursor.getString(3),
                cursor.getString(4));
        return cop;
    }

    public Cop getCopByID(int id){
        Cursor cursor = repository.getReadableDatabase()
                .rawQuery("SELECT * FROM " + KEY_TABLE_NAME + " WHERE " + KEY_ID + " = " + String.valueOf(id) + ";", null);
        return cursorToObject(cursor, 0);
    }
}
